# Pizza Illico


**date:** 13 mars au 28 mars 2020  
**sujet:**  [projet.pdf](TD-2.pdf)  
**langage:** Xamarin  
<br/><br/>


## Objectif
Faire une application cross plateforme pour commender des pizzas en vitesse. La commende de pizza est effectué à travers une API REST disponnible via le lien suivant : https://pizza.julienmialon.ovh/swagger/index.html.
<br/>

![captures d'écran](img/img1.png)
![captures d'écran](img/img2.png)
![captures d'écran](img/img3.png)
<br/><br/>


## Projet Android
projet développé uniquement pour les appareil Android. Minimum SDK 21 et SDK ciblé : 29.

### Fonctionnalitées
#### Cahier des charges
- [x] écran de connexion/inscription
- [x] modifier le profile utilisateur
- [x] modifier le mot de passe
- [x] liste des magasins triée par distance
- [x] affichage de la position des restaurants sur Google Map
- [x] affichage des pizzas d'un restaurant
- [x] panier de commande
- [x] affichage des historiques de commande

#### Bonus
- [x] authentification directe de l'utilisateur sans avoir besoins de saisir son mot de passe à chaque fois.
- [x] ajout de notre propre logo
- [ ] la déconnexion
- [ ] valider les commandes séparément (par restaurent)
- [ ] accéder à la liste des pizzas depuis Google Map

<br/><br/>


## Difficultés rencontrées
- Développer les fonctionnalités non travaillé en TP.
- Le token dans les requêtes.

<br/><br/>


## Contributeurs

- Arnaud ORLAY (@arnorlay) : Master 1 IMIS Informatique
- Marion JURÉ (@marionjure) : Master 1 IMIS Informatique
