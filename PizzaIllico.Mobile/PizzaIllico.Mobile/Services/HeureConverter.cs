﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace PizzaIllico.Mobile.Services
{
    class HeureConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            double val = (double)value;
            switch (val)
            {
                case 0:
                    return "à proximité";

                case < 60:
                    return val + " min";

                default:
                    string tmp = val % 60 < 10 ? "0" : "";
                    return $"{((int)val) / 60}h{tmp}{val % 60}";
            }
            
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            String val = (string) value;
            if (val.Contains("min"))
            {
                return Double.Parse(val.Replace("min", string.Empty));
            }
            else if (val.Contains("à proximité"))
            {
                return 0;
            }
            else
            {
                string[] l = val.Split('h');
                return Double.Parse(l[0])*60+ Int32.Parse(l[1]);
            }
        }
    }
}
