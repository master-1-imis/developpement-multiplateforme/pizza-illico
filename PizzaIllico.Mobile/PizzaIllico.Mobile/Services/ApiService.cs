using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace PizzaIllico.Mobile.Services
{
    public interface IApiService
    {
        Task<TResponse> Get<TResponse>(string url);
        Task<TResponse> Get_Parametre<TResponse>(string url, Dictionary<String, int> queryString);
        Task<TResponse> Post<TResponse>(String url, string json);
        Task<TResponse> Post_Parametre<TResponse>(string url, Dictionary<string, long> queryString, string json);
        Task<TResponse> Patch<TResponse>(String url, string json);
		string Get_Image_Parametre(string url, Dictionary<String, int> queryString);

	}
    



    public class ApiService : IApiService
    {
		// PROPERTIES : PRIVATE -----------------------------------------------
		private const string HOST = "https://pizza.julienmialon.ovh/";
        private readonly HttpClient _client = new HttpClient();



		// FUNCTIONS : GET ----------------------------------------------------
		public async Task<TResponse> Get<TResponse>(string url)
        {
			// authent
			IOAuth2Service oAuth2 = DependencyService.Get<IOAuth2Service>();
	        
			// requ�te
			HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, HOST + url);
			request.Headers.Add("authorization", $"{oAuth2.TokenType()} {oAuth2.AccessToken()}");

			// r�ponse
			HttpResponseMessage response = await _client.SendAsync(request);
			string content = await response.Content.ReadAsStringAsync();
			return JsonConvert.DeserializeObject<TResponse>(content);
		}


		public async Task<TResponse> Get_Parametre<TResponse>(string url, Dictionary<String, int> queryString)
		{
			String url_parameters = HOST + url;
			foreach (KeyValuePair<string, int> entry in queryString)
			{
				url_parameters = url_parameters.Replace(entry.Key, entry.Value + "");
			}

			// requ�te
			HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, url_parameters);

			// r�ponse
			HttpResponseMessage response = await _client.SendAsync(request);
			string content = await response.Content.ReadAsStringAsync();
			return JsonConvert.DeserializeObject<TResponse>(content);
		}


		public string Get_Image_Parametre(string url, Dictionary<String, int> queryString)
		{
			String url_parameters = HOST + url;
			foreach (KeyValuePair<string, int> entry in queryString)
			{
				url_parameters = url_parameters.Replace(entry.Key, entry.Value + "");
			}
			return url_parameters;
		}



		// FUNCTIONS : POST ---------------------------------------------------
		public async Task<TResponse> Post<TResponse>(String url, string json)
        {
			// authent
			IOAuth2Service oAuth2 = DependencyService.Get<IOAuth2Service>();

			// requ�te
			HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, HOST + url);
			request.Headers.Add("authorization", $"{oAuth2.TokenType()} {oAuth2.AccessToken()}");
			request.Content = new StringContent(json, Encoding.UTF8, "application/json"); ;

			// r�ponse
			HttpResponseMessage response = await _client.SendAsync(request);
			string content = await response.Content.ReadAsStringAsync();
			return JsonConvert.DeserializeObject<TResponse>(content);
		}


		public async Task<TResponse> Post_Parametre<TResponse>(string url, Dictionary<string, long> queryString, string json)
        {
			// authent
			IOAuth2Service oAuth2 = DependencyService.Get<IOAuth2Service>();

			// url avec paramettre
			string url_parameters = HOST + url;
			foreach (KeyValuePair<string, long> entry in queryString)
			{
				url_parameters = url_parameters.Replace(entry.Key, entry.Value + "");
			}

			// requ�te
			HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, url_parameters);
			request.Headers.Add("authorization", $"{oAuth2.TokenType()} {oAuth2.AccessToken()}");
			request.Content = new StringContent(json, Encoding.UTF8, "application/json"); ;

			// r�ponse
			HttpResponseMessage response = await _client.SendAsync(request);
			string content = await response.Content.ReadAsStringAsync();
			return JsonConvert.DeserializeObject<TResponse>(content);
		}



		// FUNCTIONS : PATCH --------------------------------------------------
		public async Task<TResponse> Patch<TResponse>(String url, string json)
        {
			// authent
			IOAuth2Service oAuth2 = DependencyService.Get<IOAuth2Service>();

			// requ�te
			HttpRequestMessage request = new HttpRequestMessage(new HttpMethod("PATCH"), HOST + url);
			request.Headers.Add("authorization", $"{oAuth2.TokenType()} {oAuth2.AccessToken()}");
			request.Content = new StringContent(json, Encoding.UTF8, "application/json"); ;

			// r�ponse
			HttpResponseMessage response = await _client.SendAsync(request);
            string content = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<TResponse>(content);
        }
	}
}