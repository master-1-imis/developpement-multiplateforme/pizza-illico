﻿using PizzaIllico.Mobile.Dtos.Pizzas;
using PizzaIllico.Mobile.Services;
using PizzaIllico.Mobile.ViewModels;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PizzaIllico.Mobile.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PizzaListPage : ContentPage
	{
		// CONSTRUCTOR --------------------------------------------------------
		public PizzaListPage()
		{
			InitializeComponent();
			BindingContext = new PizzaListViewModel();
		}


		// EVENTS -------------------------------------------------------------
		protected override void OnAppearing()
		{
			base.OnAppearing();
			if (BindingContext is PizzaListViewModel viewModel) { viewModel.DownloadListPizzas(); }
		}
	}
}