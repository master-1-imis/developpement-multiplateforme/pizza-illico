﻿using PizzaIllico.Mobile.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PizzaIllico.Mobile.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CommandHistoryPage : ContentPage
    {
        public CommandHistoryPage()
        {
            InitializeComponent();
            BindingContext = new CommandHistoryViewModel();
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            if (BindingContext is CommandHistoryViewModel viewModel) { viewModel.DownloadHistorique(); }
        }
    }
}