using PizzaIllico.Mobile.ViewModels;
using Storm.Mvvm.Forms;
using System;
using Xamarin.Forms.Xaml;

namespace PizzaIllico.Mobile.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ShopListPage
	{
		// CONSTRUCTOR --------------------------------------------------------
		public ShopListPage()
		{
			InitializeComponent();
			BindingContext = new ShopListViewModel();
		}
	}
}