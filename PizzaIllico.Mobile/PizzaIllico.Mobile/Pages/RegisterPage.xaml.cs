﻿using PizzaIllico.Mobile.Dtos.Accounts;
using PizzaIllico.Mobile.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PizzaIllico.Mobile.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class RegisterPage : ContentPage
	{
		// PROPERTIES : PRIVATE -----------------------------------------------------
		private readonly RegisterViewModel viewModel;


		// CONSTRUCTOR --------------------------------------------------------------
		public RegisterPage()
		{
			InitializeComponent();
			viewModel = new RegisterViewModel();
			BindingContext = viewModel;
		}


		// EVENEMENTS ---------------------------------------------------------
		private void SaveForm(object sender, EventArgs e)
		{
			// création de l'utilisateur
			CreateUserRequest userRequest = new CreateUserRequest();
			userRequest.ClientId = "MOBILE";
			userRequest.ClientSecret = "UNIV";
			userRequest.FirstName = firstname.Text;
			userRequest.LastName = lastname.Text;
			userRequest.Email = email.Text;
			userRequest.PhoneNumber = phone.Text;
			userRequest.Password = password.Text;

			// mise à jour de la propriété dans le ViewModel
			viewModel.CreateUserRequest = userRequest;

			// Exécution de la méthode de création d'un nouvel utilisateur
			viewModel.CreatedUser();
		}
	}
}