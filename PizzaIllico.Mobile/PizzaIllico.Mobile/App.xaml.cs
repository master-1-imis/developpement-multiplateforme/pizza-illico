﻿using System.Diagnostics;
using PizzaIllico.Mobile.Pages;
using PizzaIllico.Mobile.Services;
using Storm.Mvvm;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]

namespace PizzaIllico.Mobile
{
    public partial class App : MvvmApplication
    {
        public App() : base(() => new OAuth2Service().IsExpired() ? new SignInPage() : new MainPage(), RegisterServices)
        {
#if DEBUG
            Log.Listeners.Add(new DelegateLogListener((arg1, arg2) => Debug.WriteLine($"{arg1} : {arg2}")));
#endif
            InitializeComponent();
        }

        private static void RegisterServices()
        {
            // Authentification
            DependencyService.RegisterSingleton<IOAuth2Service>(new OAuth2Service());

            // Panier de commande
            DependencyService.RegisterSingleton<IBasketService>(new BasketService());
            
            // Requête HTTP
            DependencyService.RegisterSingleton<IApiService>(new ApiService());
            
            // Requête avec l'API de pizza
            DependencyService.RegisterSingleton<IPizzaApiService>(new PizzaApiService());
        }
    }
}