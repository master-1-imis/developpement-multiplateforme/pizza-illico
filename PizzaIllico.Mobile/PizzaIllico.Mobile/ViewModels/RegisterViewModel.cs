﻿using PizzaIllico.Mobile.Dtos;
using PizzaIllico.Mobile.Dtos.Accounts;
using PizzaIllico.Mobile.Dtos.Authentications;
using PizzaIllico.Mobile.Services;
using Plugin.Toast;
using Storm.Mvvm;
using Storm.Mvvm.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace PizzaIllico.Mobile.ViewModels
{
	class RegisterViewModel : ViewModelBase
	{

		// PROPERTIES : PRIVATE -----------------------------------------------------
		private CreateUserRequest _createUserRequest;
		private LoginResponse _loginResponse;


		// COMMANDES : DEFINITION ---------------------------------------------------
		public ICommand BackRegister { get; }
		public ICommand CreatedCommand { get; }


		// PROPERTIES : EXPOSED -----------------------------------------------------
		public CreateUserRequest CreateUserRequest
		{
			get => _createUserRequest;
			set => SetProperty(ref _createUserRequest, value);
		}

		public LoginResponse LoginResponse
		{
			get => _loginResponse;
			set => SetProperty(ref _loginResponse, value);
		}


		// CONSTRUCTOR --------------------------------------------------------------
		public RegisterViewModel()
		{
			BackRegister = new Command(BackAction);
			CreatedCommand = new Command(CreatedUser);
		}


		// COMMANDES : IMPLEMENTATION -----------------------------------------------
		public async void CreatedUser()
		{
			IPizzaApiService service = DependencyService.Get<IPizzaApiService>();
			Response<LoginResponse> response = await service.CreateUser(CreateUserRequest);
			if (response.IsSuccess)
			{
				LoginResponse = response.Data;
				BackAction();
				CrossToastPopUp.Current.ShowToastMessage("Sucess");
			}
			else
			{
				switch (response.ErrorCode)
				{
					case "GENERIC_HTTP_ERROR":
						CrossToastPopUp.Current.ShowToastMessage("Error : empty field");
						break;

					default:
						CrossToastPopUp.Current.ShowToastMessage("Error");
						break;
				}
			}
		}
		

		private void BackAction()
		{
			INavigationService navigationService = DependencyService.Get<INavigationService>();
			navigationService.PopAsync();
		}
	}
}
