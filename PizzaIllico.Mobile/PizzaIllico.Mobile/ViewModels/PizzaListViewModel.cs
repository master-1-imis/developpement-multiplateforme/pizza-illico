using Storm.Mvvm;
using System.Collections.ObjectModel;
using System.Windows.Input;
using PizzaIllico.Mobile.Dtos.Pizzas;
using Xamarin.Forms;
using PizzaIllico.Mobile.Services;
using System.Collections.Generic;
using PizzaIllico.Mobile.Dtos;
using Storm.Mvvm.Navigation;
using System;
using Plugin.Toast;
using PizzaIllico.Mobile.Dtos.Authentications;

namespace PizzaIllico.Mobile.ViewModels
{
	class PizzaListViewModel : ViewModelBase
	{
		// PROPERTIES : PRIVATE -----------------------------------------------
		private ObservableCollection<PizzaItem> _pizzas;
		private ShopItem _shop;



		// COMMANDES : DEFINITION ---------------------------------------------
		public ICommand AddPizzaInBasketCommand { get; }



		// PROPERTIES : EXPOSED -----------------------------------------------
		public ObservableCollection<PizzaItem> Pizzas
		{
			get => _pizzas;
			set => SetProperty(ref _pizzas, value);
		}

		[NavigationParameter]
		public ShopItem Shop
		{
			get => _shop;
			set => SetProperty(ref _shop, value);
		}



		// CONSTRUCTOR --------------------------------------------------------
		public PizzaListViewModel()
		{
			// comandes
			AddPizzaInBasketCommand = new Command<PizzaItem>(AddPizzaInBasketAction);
		}



		// COMMANDES : IMPLEMENTATION -----------------------------------------
		private void AddPizzaInBasketAction(PizzaItem pizza)
		{
			// panier de commande
			IBasketService basket = DependencyService.Get<IBasketService>();

			// ajouter dans le panier
			if (basket.Add(Shop, pizza))
			{
				CrossToastPopUp.Current.ShowToastMessage("Pizza ajouté au panier");
			}
			else
			{
				CrossToastPopUp.Current.ShowToastMessage("Pizza déjà dans le panier");
			}
		}



		// EVENEMENTS ---------------------------------------------------------
		public async void DownloadListPizzas()
		{
			IPizzaApiService service = DependencyService.Get<IPizzaApiService>();
			IOAuth2Service auth2 = DependencyService.Get<IOAuth2Service>();
			if (auth2.CheckExpired()) {
				RefreshRequest refreshRequest = auth2.getRefreshRequest();
				Response<LoginResponse> response1 = await service.Refresh(refreshRequest);
				if (response1.IsSuccess)
				{
					auth2.SaveToken(response1.Data);
				}
			}
			// met � jour la liste des pizzas
			
			Response<List<PizzaItem>> response = await service.ListPizzas((int)Shop.Id);
			if (response.IsSuccess) {
				Pizzas = new ObservableCollection<PizzaItem>(response.Data);

				foreach (PizzaItem pizza in Pizzas) 
				{
					pizza.Image = service.GetImagePizza((int)Shop.Id, (int)pizza.Id);
				}
			}
		}

	}
}
