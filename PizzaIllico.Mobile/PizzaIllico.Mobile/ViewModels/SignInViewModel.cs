﻿using Storm.Mvvm;
using Storm.Mvvm.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;
using PizzaIllico.Mobile.Pages;
using PizzaIllico.Mobile.Dtos.Authentications.Credentials;
using PizzaIllico.Mobile.Dtos.Authentications;
using PizzaIllico.Mobile.Services;
using PizzaIllico.Mobile.Dtos;
using Xamarin.Essentials;
using System.Threading.Tasks;
using Plugin.Toast;

namespace PizzaIllico.Mobile.ViewModels
{
	class SignInViewModel : ViewModelBase
	{
		// PROPERTIES : PRIVATE -----------------------------------------------
		private LoginResponse _loginResponse;
		private IOAuth2Service oAuth2;



		// COMMANDES : DEFINITION ---------------------------------------------
		public ICommand GoMain { get; }
		public ICommand GoRegister { get; }

		

		// PROPERTIES : EXPOSED -----------------------------------------------
		public LoginResponse LoginResponse
		{
			get => _loginResponse;
			set => SetProperty(ref _loginResponse, value);
		}



		// CONSTRUCTOR --------------------------------------------------------
		public SignInViewModel()
		{
			// services
			oAuth2 = DependencyService.Get<IOAuth2Service>();

			// commandes
			GoMain = new Command(GoMainAction);
			GoRegister = new Command(GoRegisterAction);
		}



		// COMMANDES : IMPLEMENTATION -----------------------------------------
		private void GoRegisterAction()
		{
			INavigationService navigationService = DependencyService.Get<INavigationService>();
			navigationService.PushAsync<RegisterPage>();
		}


		private void GoMainAction()
		{
		   INavigationService navigationService = DependencyService.Get<INavigationService>();
		   navigationService.PushAsync<MainPage>();
		}


		public async void AuthentifyUser(LoginWithCredentialsRequest LoginRequest)
		{
			// récupération du service
			IPizzaApiService service = DependencyService.Get<IPizzaApiService>();

			// authentification de l'utilisateur auprès de l'API
			Response<LoginResponse> response = await service.AuthentifyUser(LoginRequest);

			if (response.IsSuccess)
			{
				// sauvegarde du token
				oAuth2.SaveToken(response.Data);
				GoMainAction();
			}
			else
			{
				switch (response.ErrorCode)
				{
					case "GENERIC_HTTP_ERROR":
						CrossToastPopUp.Current.ShowToastMessage("Error: email or  password");
						break;

					default:
						CrossToastPopUp.Current.ShowToastMessage("Error");
						break;
				}
			}
		}
    }
}
