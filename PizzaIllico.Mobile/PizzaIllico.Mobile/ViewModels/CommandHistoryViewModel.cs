﻿using Storm.Mvvm;
using Storm.Mvvm.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;
using System.Collections.ObjectModel;
using PizzaIllico.Mobile.Dtos.Pizzas;
using PizzaIllico.Mobile.Services;
using PizzaIllico.Mobile.Dtos;
using System.Threading.Tasks;
using Newtonsoft.Json;
using PizzaIllico.Mobile.Dtos.Authentications;

namespace PizzaIllico.Mobile.ViewModels
{
    class CommandHistoryViewModel : ViewModelBase
    {
        private ObservableCollection<OrderItem> _orders;
        public ICommand BackCommandHistory { get; }

        public CommandHistoryViewModel()
        {
            BackCommandHistory = new Command(BackAction);
        }
        public ObservableCollection<OrderItem> Orders
        {
            get => _orders;
            set => SetProperty(ref _orders, value);
        }


        private void BackAction(object obj)
        {
            INavigationService navigationService = DependencyService.Get<INavigationService>();
            navigationService.PopAsync();
        }
        public  async void DownloadHistorique()
        {
            IPizzaApiService service = DependencyService.Get<IPizzaApiService>();
            IOAuth2Service auth2 = DependencyService.Get<IOAuth2Service>();
            if (auth2.CheckExpired())
            {
                RefreshRequest refreshRequest = auth2.getRefreshRequest();
                Response<LoginResponse> response1 = await service.Refresh(refreshRequest);
                if (response1.IsSuccess)
                {
                    auth2.SaveToken(response1.Data);
                }
            }
            // met à jour historique
            Response<List<OrderItem>> response = await service.ListOrders();
            if (response.IsSuccess) {
                List<OrderItem> tmp = response.Data;
                tmp.Sort((x, y) => -x.Date.CompareTo(y.Date));
                Orders = new ObservableCollection<OrderItem>(tmp);
            }
        }
    }
}
