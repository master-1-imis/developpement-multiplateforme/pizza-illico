using Exceptionless.Models.Collections;
using PizzaIllico.Mobile.Dtos;
using PizzaIllico.Mobile.Dtos.Authentications;
using PizzaIllico.Mobile.Dtos.Pizzas;
using PizzaIllico.Mobile.Services;
using Plugin.Toast;
using Storm.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace PizzaIllico.Mobile.ViewModels
{
    class CommandValidationViewModel : ViewModelBase
    {
        // PROPERTIES : PRIVATE -----------------------------------------------
        private IBasketService basket;
        private CreateOrderRequest _createOrderRequest;
        private ObservableCollection<PizzaItem> _pizzas;
        private double _totalCost;
        private bool _canValidate;



        // COMMANDES : DEFINITION ---------------------------------------------
        public ICommand RemovePizzaCommand { get; }
        public ICommand ValidateOrderCommand { get; }



        // PROPERTIES : EXPOSED -----------------------------------------------
        public CreateOrderRequest CreateOrderRequest
        {
            get => _createOrderRequest;
            set => SetProperty(ref _createOrderRequest, value);
        }


        public ObservableCollection<PizzaItem> Pizzas
        {
            get => _pizzas;
            set => SetProperty(ref _pizzas, value);
        }


        public double TotalCost
        {
            get => _totalCost;
            set => SetProperty(ref _totalCost, value);
        }


        public bool CanValidate
        {
            get => _canValidate;
            set => SetProperty(ref _canValidate, value);
        }



        // CONSTRUCTOR --------------------------------------------------------
        public CommandValidationViewModel()
        {
            // panier de commande
            basket = DependencyService.Get<IBasketService>();

            // commandes
            RemovePizzaCommand = new Command<PizzaItem>(RemovePizzaAction);
            ValidateOrderCommand = new Command(ValidateOrderAction);

            CanValidate = false;
        }



        // COMMANDES : IMPLEMENTATION -----------------------------------------
        private void RemovePizzaAction(PizzaItem pizza)
        {
            foreach (ShopItem shop in basket.Shops())
            {
                if (basket.Contain(shop, pizza))
                {
                    basket.Remove(shop, pizza);
                    Update();
                    break;
                }
            }
        }


        private async void ValidateOrderAction()
        {
            IPizzaApiService service = DependencyService.Get<IPizzaApiService>();
            foreach (ShopItem shop in basket.Shops())
            {
                CreateOrderRequest orderRequest = new CreateOrderRequest();
                orderRequest.PizzaIds = new List<long>();
                foreach (PizzaItem pizza in basket.Pizzas(shop))
                {
                    orderRequest.PizzaIds.Add(pizza.Id);
                }
                Response<OrderItem> response = await service.ValidateCommand(shop.Id, orderRequest);

                if (response.IsSuccess)
                {
                    CrossToastPopUp.Current.ShowToastMessage($"Sended for : {shop.Name}");
                    basket.Remove(shop);
                    Update();
                }
                else
                {
                    CrossToastPopUp.Current.ShowToastMessage($"Error for : {shop.Name}");
                }
            }
        }



        // FUNCTIONS ----------------------------------------------------------
        public void Update()
        {
            Expired();

            List<PizzaItem> pizzas = basket.Pizzas();
            pizzas.Sort((x, y) => x.Name.CompareTo(y.Name));

            // mettre � jour la liste de pizza
            Pizzas = new ObservableCollection<PizzaItem>(pizzas);

            // mettre � jour le prix
            double cost = 0;
            foreach (PizzaItem pizza in pizzas)
            {
                cost += pizza.Price;
            }
            TotalCost = cost;

            // activer/desactiver le bouton de validation
            CanValidate = cost > 0.0;
        }
        public async void Expired() {

            IPizzaApiService service = DependencyService.Get<IPizzaApiService>();
            IOAuth2Service auth2 = DependencyService.Get<IOAuth2Service>();
            if (auth2.CheckExpired())
            {
                RefreshRequest refreshRequest = auth2.getRefreshRequest();
                Response<LoginResponse> response1 = await service.Refresh(refreshRequest);
                if (response1.IsSuccess)
                {
                    auth2.SaveToken(response1.Data);
                }
            }
        }        
    }
}
